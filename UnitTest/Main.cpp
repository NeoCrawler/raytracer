#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"
#include "Matrix3x3.h"
#include "Vector3.h"
#include "MyMath.h"

#include "Material.h"
#include "Sphere.h"
#include "Cube.h"
#include "Plane.h"
#include "Hit.h"
#include "List.h"

TEST_CASE("Testing Vector3 functionality")
{
	 //I checked resulting whole or 1 decimel numbers to avoid the inaccuracy of floating point precision.

	SUBCASE("Construction")
	{
		/// Default constructor with default parameters.
		const Vector3 v0;
		CHECK(v0.x == 0.0f);
		CHECK(v0.y == 0.0f);

		/// Constructor from components
		const Vector3 v1(2.3f, 5.6f, 12.f);
		CHECK(v1.x == 2.3f);
		CHECK(v1.y == 5.6f);
		CHECK(v1.z == 12.f);

		/// Copy construction
		Vector3 v2(v1);
		CHECK(v2.x == v1.x);
		CHECK(v2.y == v1.y);
		CHECK(v2.z == v1.z);
	}

	SUBCASE("Operators")
	{
		// Add
		const Vector3 v1 = Vector3(2, 5.5f, 6) + Vector3(3, 3, 6);
		CHECK(v1.x == 5);
		CHECK(v1.y == 8.5f);
		CHECK(v1.z == 12);

		// Substract
		const Vector3 v2 = Vector3(2, 5.5f, 6) - Vector3(3, 3, 6);
		CHECK(v2.x == -1);
		CHECK(v2.y == 2.5f);
		CHECK(v2.z == 0);

		// Multiply
		const Vector3 v3 = Vector3(1, 2, 3) * 3;
		CHECK(v3.x == 3);
		CHECK(v3.y == 6);
		CHECK(v3.z == 9);

		// Multiply by Vector3
		const Vector3 v4 = Vector3(1, 2, 3) * Vector3(3, 3, 3);
		CHECK(v4.x == 3);
		CHECK(v4.y == 6);
		CHECK(v4.z == 9);

		// Devision
		const Vector3 v5 = Vector3(3, 6, 9) / 3;
		CHECK(v5.x == 1);
		CHECK(v5.y == 2);
		CHECK(v5.z == 3);

		// Add
		Vector3 v6;
		v6 += Vector3(1, 1, 1);
		CHECK(v6.x == 1);
		CHECK(v6.y == 1);
		CHECK(v6.z == 1);

		// Substract
		Vector3 v7;
		v7 -= Vector3(1, 1, 1);
		CHECK(v7.x == -1);
		CHECK(v7.y == -1);
		CHECK(v7.z == -1);

		// Multiply
		Vector3 v8;
		v8 *= 2;
		CHECK(v8.x == 0);
		CHECK(v8.y == 0);
		CHECK(v8.z == 0);

		// Devision
		Vector3 v9;
		v9 /= 2;
		CHECK(v9.x == 0);
		CHECK(v9.y == 0);
		CHECK(v9.z == 0);

		// True
		const bool r1 = Vector3(1, 2, 3) == Vector3(1, 2, 3);
		CHECK(r1 == true);

		// False
		const bool r2 = Vector3(1, 2, 3) == Vector3(1, 5, 3);
		CHECK(r2 != true);
	}

	SUBCASE("Functions")
	{
		// Scale
		Vector3 v1 = Vector3(2, 3, 4);
		v1.Scale(Vector3(2, 2, 2));
		CHECK(v1.x == 4);
		CHECK(v1.y == 6);
		CHECK(v1.z == 8);

		// Magnitude
		const Vector3 v2 = Vector3(4, 8, 8);
		CHECK(v2.Magnitude() == 12);

		// Magnitude
		const Vector3 v3 = Vector3(4, 8, 8);
		CHECK(v3.SquareMagnitude() == 144);

		// Normalize
		Vector3 v4 = Vector3(0, 0, 1);
		v4.Normalize();
		CHECK(v4.x == 0);
		CHECK(v4.y == 0);
		CHECK(v4.z == 1);

		// Normalized
		const Vector3 v5 = Vector3(0, 0, 1).Normalized();
		CHECK(v5.x == 0);
		CHECK(v5.y == 0);
		CHECK(v5.z == 1);

		// Inverse
		const Vector3 v6 = Vector3(1, 1, 1).Inverse();
		CHECK(v6.x == -1);
		CHECK(v6.y == -1);
		CHECK(v6.z == -1);
	}

	SUBCASE("Static Functions")
	{
		// Dot
		const float v1 = Vector3::Dot(Vector3(1, 1, 1), Vector3(2, 2, 2));
		CHECK(v1 == 6);

		// Cross
		const Vector3 v2 = Vector3::Cross(Vector3(1, 1, 1), Vector3(2, 3, 4));
		CHECK(v2.x == 1);
		CHECK(v2.y == -2);
		CHECK(v2.z == 1);

		// Distance
		const float v3 = Vector3::Distance(Vector3(0, 1, 0), Vector3(0, 2, 0));
		CHECK(v3 == 1);

		// Vector zero
		const Vector3 v4 = Vector3::Zero();
		CHECK(v4.x == 0);
		CHECK(v4.y == 0);
		CHECK(v4.z == 0);

		// Vector up
		const Vector3 v5 = Vector3::Up();
		CHECK(v5.y == 1);

		// Vector right
		const Vector3 v6 = Vector3::Right();
		CHECK(v6.x == 1);

		// Vector forward
		const Vector3 v7 = Vector3::Forward();
		CHECK(v7.z == 1);
	}
}

TEST_CASE("Testing Matrix3x3 functionality")
{
	Matrix3x3 m1 = Matrix3x3(2, 4, 8, 16, 32, 64, 128, 256, 512);
	Matrix3x3 m2 = Matrix3x3(2, 6, -3, 9, 8, -23, 14, -16, 204);
	Matrix3x3 m3;

	// Addition.
	m3 = m1 + m2;

	CHECK(m3.GetMatrix()[0] == 4); CHECK(m3.GetMatrix()[1] == 10); CHECK(m3.GetMatrix()[2] == 5);
	CHECK(m3.GetMatrix()[3] == 25); CHECK(m3.GetMatrix()[4] == 40); CHECK(m3.GetMatrix()[5] == 41);
	CHECK(m3.GetMatrix()[6] == 142); CHECK(m3.GetMatrix()[7] == 240); CHECK(m3.GetMatrix()[8] == 716);

	// Substraction.
	m3 = m3 - m2;

	CHECK(m3.GetMatrix()[0] == 2); CHECK(m3.GetMatrix()[1] == 4); CHECK(m3.GetMatrix()[2] == 8);
	CHECK(m3.GetMatrix()[3] == 16); CHECK(m3.GetMatrix()[4] == 32); CHECK(m3.GetMatrix()[5] == 64);
	CHECK(m3.GetMatrix()[6] == 128); CHECK(m3.GetMatrix()[7] == 256); CHECK(m3.GetMatrix()[8] == 512);

	// Multiplication.
	m3 = m1 * m2;

	CHECK(m3.GetMatrix()[0] == 152); CHECK(m3.GetMatrix()[1] == -84); CHECK(m3.GetMatrix()[2] == 1534);
	CHECK(m3.GetMatrix()[3] == 1216); CHECK(m3.GetMatrix()[4] == -672); CHECK(m3.GetMatrix()[5] == 12272);
	CHECK(m3.GetMatrix()[6] == 9728); CHECK(m3.GetMatrix()[7] == -5376); CHECK(m3.GetMatrix()[8] == 98176);

	// Determinant
	CHECK(m3.Determinant() == 0);

	// Identity.
	m3 = m3.Identity();

	CHECK(m3.GetMatrix()[0] == 1); CHECK(m3.GetMatrix()[1] == 0); CHECK(m3.GetMatrix()[2] == 0);
	CHECK(m3.GetMatrix()[3] == 0); CHECK(m3.GetMatrix()[4] == 1); CHECK(m3.GetMatrix()[5] == 0);
	CHECK(m3.GetMatrix()[6] == 0); CHECK(m3.GetMatrix()[7] == 0); CHECK(m3.GetMatrix()[8] == 1);
}

TEST_CASE("Testing Math functionality")
{
	float f1 = 1;
	f1 = Math::Remap(f1, 0, 1, 0, 200);

	CHECK(f1 == 200);

	f1 = Math::Clamp(f1, 0, 1);

	CHECK(f1 == 1);

	f1 = Math::Lerp(0, 1, 1);

	CHECK(f1 == 1);

	f1 = Math::Sign(f1);

	CHECK(f1 == 1);

	f1 = Math::Max(f1, 1);

	CHECK(f1 == 1);

	f1 = Math::Min(f1, 0);

	CHECK(f1 == 0);

	Math::Swap(f1, f1);

	CHECK(f1 == 0);
}

TEST_CASE("Testing List functionality")
{
	List<int> l1 = List<int>(5);

	CHECK(l1.GetSize() == 5);

	l1.Add(2);

	CHECK(l1.GetSize() == 6);

	l1.Remove(3);
	l1.Remove(2);

	CHECK(l1.GetSize() == 4);

	l1.Clear();
	
	CHECK(l1.GetSize() == 0);
}

TEST_CASE("Testing List functionality")
{
	Sphere s1 = Sphere(Vector3(0, 0, 10), new Material(), 2);
	Plane p1 = Plane(Vector3(0, 0, 10) , new Material(), Vector3::Forward().Inverse());
	Cube c1 = Cube(Vector3(0, 0, 10), new Material(), Vector3(1, 1, 1));

	Vector3 origin = Vector3();
	Vector3 direction = Vector3(0, 0, 1);
	Hit hit;

	s1.Intersect(origin, direction, &hit);

	CHECK(hit.hasHit == true);

	p1.Intersect(origin, direction, &hit);

	CHECK(hit.hasHit == true);

	c1.Intersect(origin, direction, &hit);

	CHECK(hit.hasHit == true);

}