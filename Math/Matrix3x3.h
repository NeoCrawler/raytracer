#pragma once
#include <stdio.h>

class Matrix3x3
{

private:

	float* matrix;

public:
	
	Matrix3x3();

	Matrix3x3(float t_matrix[9]);

	Matrix3x3(float t_1, float t_2, float t_3, float t_4, float t_5, float t_6, float t_7, float t_8, float t_9);

	Matrix3x3 operator+(const Matrix3x3& t_m) const;

	Matrix3x3 operator-(const Matrix3x3& t_m) const;
	Matrix3x3 operator*(const Matrix3x3& t_m) const;

	Matrix3x3 Identity() const;

	float Determinant() const;

	float* GetMatrix() const;

	void Print();
};

