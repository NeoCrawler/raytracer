#pragma once
#include <cmath>

// Left hand coordination.
class Vector3
{
public:

	float x;
	float y;
	float z;

public:

	Vector3();
	Vector3(float x, float y, float z);

	Vector3 operator+(const Vector3& v) const;

	Vector3 operator-(const Vector3& v) const;

	Vector3 operator*(const float scalar) const;

	Vector3 operator*(const Vector3 v) const;

	Vector3 operator/(const float scalar) const;

	Vector3 operator/(const Vector3 v) const;

	void operator+=(const Vector3& v);

	void operator-=(const Vector3& v);

	void operator*=(const float scalar);

	void operator/=(const float scalar);

	bool operator==(const Vector3& other) const;

	bool operator!=(const Vector3& other) const;

	void Scale(const Vector3& v);

	float Magnitude() const;

	float SquareMagnitude() const;

	void Normalize();

	Vector3 Normalized() const;

	Vector3 Inverse() const;

	static float Dot(const Vector3& lhs, const Vector3& rhs);

	static Vector3 Cross(const Vector3& lhs, const Vector3& rhs);

	static float Distance(const Vector3& lhs, const Vector3& rhs);

	static Vector3 Zero();

	static Vector3 Up();

	static Vector3 Right();

	static Vector3 Forward();
};