#include "MyMath.h"

// Linear interpolate between two values.
float Math::Lerp(const float t_from, const float t_to, const float t_time)
{
	return (t_from * (1.0f - t_time)) + (t_to * t_time);
}

// Remap float value from range to range.
float Math::Remap(const float& t_value, const float t_from1, const float t_from2, const float t_to1, const float t_to2)
{
	return t_to1 + (t_value - t_from1) * (t_to2 - t_to1) / (t_from2 - t_from1);
}

// Clamp value between two values.
float Math::Clamp(const float& t_value, const float t_min, const float t_max)
{
	if (t_value > t_max)
		return t_max;
	else if (t_value < t_min)
		return t_min;
	else
		return t_value;
}

// Returns the highest of the two values.
float Math::Max(const float& t_value, const float t_max)
{
	if (t_max > t_value)
		return t_max;
	else
		return t_value;
}

// Returns the lowest of the two values.
float Math::Min(const float& t_value, const float t_min)
{
	if (t_min < t_value)
		return t_min;
	else
		return t_value;
}

void Math::Swap(float& t_lhs, float& t_rhs)
{
	float _temp = t_lhs;
	t_lhs = t_rhs;
	t_rhs = _temp;
}

float Math::Sign(const float& t_value)
{
	if (t_value > 0)
		return 1;
	else
		return-1;
}