﻿#pragma once

// π = Pi
constexpr long double Pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067;

class Math
{
public:

	// Linear interpolate between two values.
	static float Lerp(const float t_from, const float t_to, const float t_time);

	// Remap float value from range to range.
	static float Remap(const float& t_value, const float t_from1, const float t_from2, const float t_to1, const float t_to2);

	// Clamp value between two values.
	static float Clamp(const float& t_value, const float t_min, const float t_max);

	// Returns the highest of the two values.
	static float Max(const float& t_value, const float t_max);

	// Returns the lowest of the two values.
	static float Min(const float& t_value, const float t_min);

	static void Swap(float& t_lhs, float& t_rhs);

	static float Sign(const float& t_value);
};

