#include "Matrix3x3.h"

Matrix3x3::Matrix3x3()
{
	matrix = new float[9];

	for (size_t i = 0; i < 9; i++)
	{
		matrix[i] = 0;
	}
}

Matrix3x3::Matrix3x3(float t_matrix[9])
{
	matrix = new float[9];

	for (size_t i = 0; i < 9; i++)
	{
		matrix[i] = t_matrix[i];
	}
}

Matrix3x3::Matrix3x3(float t_1, float t_2, float t_3, float t_4, float t_5, float t_6, float t_7, float t_8, float t_9)
{
	matrix = new float[9];
	matrix[0] = t_1;
	matrix[1] = t_2;
	matrix[2] = t_3;
	matrix[3] = t_4;
	matrix[4] = t_5;
	matrix[5] = t_6;
	matrix[6] = t_7;
	matrix[7] = t_8;
	matrix[8] = t_9;
}

Matrix3x3 Matrix3x3::operator+(const Matrix3x3& t_m) const
{
	Matrix3x3 _temp;

	_temp.GetMatrix()[0] = matrix[0] + t_m.GetMatrix()[0];
	_temp.GetMatrix()[1] = matrix[1] + t_m.GetMatrix()[1];
	_temp.GetMatrix()[2] = matrix[2] + t_m.GetMatrix()[2];
	_temp.GetMatrix()[3] = matrix[3] + t_m.GetMatrix()[3];
	_temp.GetMatrix()[4] = matrix[4] + t_m.GetMatrix()[4];
	_temp.GetMatrix()[5] = matrix[5] + t_m.GetMatrix()[5];
	_temp.GetMatrix()[6] = matrix[6] + t_m.GetMatrix()[6];
	_temp.GetMatrix()[7] = matrix[7] + t_m.GetMatrix()[7];
	_temp.GetMatrix()[8] = matrix[8] + t_m.GetMatrix()[8];

	return _temp;
}

Matrix3x3 Matrix3x3::operator-(const Matrix3x3& t_m) const
{
	Matrix3x3 _temp;

	_temp.GetMatrix()[0] = matrix[0] - t_m.GetMatrix()[0];
	_temp.GetMatrix()[1] = matrix[1] - t_m.GetMatrix()[1];
	_temp.GetMatrix()[2] = matrix[2] - t_m.GetMatrix()[2];
	_temp.GetMatrix()[3] = matrix[3] - t_m.GetMatrix()[3];
	_temp.GetMatrix()[4] = matrix[4] - t_m.GetMatrix()[4];
	_temp.GetMatrix()[5] = matrix[5] - t_m.GetMatrix()[5];
	_temp.GetMatrix()[6] = matrix[6] - t_m.GetMatrix()[6];
	_temp.GetMatrix()[7] = matrix[7] - t_m.GetMatrix()[7];
	_temp.GetMatrix()[8] = matrix[8] - t_m.GetMatrix()[8];

	return _temp;
}

Matrix3x3 Matrix3x3::operator*(const Matrix3x3& t_m) const
{
	Matrix3x3 _temp;

	_temp.GetMatrix()[0] = matrix[0] * t_m.GetMatrix()[0] + matrix[1] * t_m.GetMatrix()[3] + matrix[2] * t_m.GetMatrix()[6];
	_temp.GetMatrix()[1] = matrix[0] * t_m.GetMatrix()[1] + matrix[1] * t_m.GetMatrix()[4] + matrix[2] * t_m.GetMatrix()[7];
	_temp.GetMatrix()[2] = matrix[0] * t_m.GetMatrix()[2] + matrix[1] * t_m.GetMatrix()[5] + matrix[2] * t_m.GetMatrix()[8];

	_temp.GetMatrix()[3] = matrix[3] * t_m.GetMatrix()[0] + matrix[4] * t_m.GetMatrix()[3] + matrix[5] * t_m.GetMatrix()[6];
	_temp.GetMatrix()[4] = matrix[3] * t_m.GetMatrix()[1] + matrix[4] * t_m.GetMatrix()[4] + matrix[5] * t_m.GetMatrix()[7];
	_temp.GetMatrix()[5] = matrix[3] * t_m.GetMatrix()[2] + matrix[4] * t_m.GetMatrix()[5] + matrix[5] * t_m.GetMatrix()[8];

	_temp.GetMatrix()[6] = matrix[6] * t_m.GetMatrix()[0] + matrix[7] * t_m.GetMatrix()[3] + matrix[8] * t_m.GetMatrix()[6];
	_temp.GetMatrix()[7] = matrix[6] * t_m.GetMatrix()[1] + matrix[7] * t_m.GetMatrix()[4] + matrix[8] * t_m.GetMatrix()[7];
	_temp.GetMatrix()[8] = matrix[6] * t_m.GetMatrix()[2] + matrix[7] * t_m.GetMatrix()[5] + matrix[8] * t_m.GetMatrix()[8];

	return _temp;
}

Matrix3x3 Matrix3x3::Identity() const
{
	float _temp[9] = { 1,0,0,0,1,0,0,0,1 };
	return Matrix3x3(_temp);
}

float Matrix3x3::Determinant() const
{
	float _a = matrix[0] * (matrix[4] * matrix[8] - matrix[5] * matrix[7]);
	float _b = matrix[1] * (matrix[3] * matrix[8] - matrix[5] * matrix[6]);
	float _c = matrix[2] * (matrix[3] * matrix[7] - matrix[4] * matrix[6]);

	return _a - _b + _c;
}

float* Matrix3x3::GetMatrix() const
{
	return matrix;
}

void Matrix3x3::Print()
{
	printf("%f, %f, %f\n", matrix[0], matrix[1], matrix[2]);
	printf("%f, %f, %f\n", matrix[3], matrix[4], matrix[5]);
	printf("%f, %f, %f\n", matrix[6], matrix[7], matrix[8]);
}