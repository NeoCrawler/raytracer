#pragma once
#include "Shape.h"
class Cube : public Shape
{
private:
	Vector3 min[2];
	Vector3 size;

public:
	Cube(Vector3 t_position, Material* t_material, Vector3 t_size);

	// Inherited via Shape
	virtual bool Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* t_hit) override;

	// Inherited via Shape
	virtual Box GetBoundingBox() override;
};

