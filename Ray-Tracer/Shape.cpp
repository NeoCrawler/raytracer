#include "Shape.h"

Shape::Shape(Vector3 t_position, Material* t_material) : GameObject(t_position)
{
	type = ShapeType::None;
	material = t_material;
}

const ShapeType Shape::GetShapeType() const
{
	return type;
}

const Material* Shape::GetMaterial() const
{
	return material;
}
