#pragma once

#include "GameObject.h"

class Box : public GameObject
{

private:

	Shape* shape;
	

public:

	Vector3 min, max;

	Box();
	Box(Vector3 t_position, Vector3 t_min, Vector3 t_max);
	bool Intersect(Vector3 t_origin, Vector3 t_direction);
	Shape* GetShape();
	void SetShape(Shape* t_shape);
};

