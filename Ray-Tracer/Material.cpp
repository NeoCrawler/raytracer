#include "Material.h"
#include "Vector3.h"
void Material::SetChecker(bool t_isChecker)
{
	isChecker = t_isChecker;
}

Material::Material()
{
	diffuse = Color(0, 0, 0, 255);
	ambient = Color(0, 0, 0, 255);
	specular = Color(0, 0, 0, 255);
	reflection = Color(0, 0, 0, 255);
	emission = Color(0, 0, 0, 255);
	isChecker = false;
}

Material::Material(Color t_diffuse, Color t_ambient, Color t_specular, Color t_reflection, Color t_emission)
{
	diffuse = t_diffuse;
	ambient = t_ambient;
	specular = t_specular;
	reflection = t_reflection;
	emission = t_emission;
	isChecker = false;
}

const Color& Material::GetDiffuse(const Vector3 t_point) const
{
	if (isChecker)
	{
		Color _temp;
		//Vector3 _p1 = _direction * _result;
		int _x = static_cast<int>(fabsf(t_point.x - 10000) / 1);
		int _z = static_cast<int>(fabsf(t_point.z - 10000) / 1 * 1.7f);

		_x += _z % 2;

		if (_x % 2 == 1)
		{
			_temp = Color(255, 255, 255, 255);
		}
		else
		{
			_temp = Color(0, 0, 0, 255);
		}

		return _temp;
	}
	else
	{
		return diffuse;
	}
}

const Color& Material::GetAmbient() const
{
	return ambient;
}

const Color& Material::GetSpecular() const
{
	return specular;
}

const Color& Material::GetEmission() const
{
	return emission;
}

const Color& Material::GetReflection() const
{
	return reflection;
}
