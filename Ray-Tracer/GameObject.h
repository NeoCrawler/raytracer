#pragma once
#include <iostream>

#include "Vector3.h"
#include "MyMath.h"
#include "Matrix3x3.h"
#include "Material.h"
#include "Hit.h"

class GameObject
{

private:

	Vector3 position;

public:
	
	GameObject(Vector3 t_position);
	void SetPosition(Vector3 t_position = Vector3(0, 0, 0));
	Vector3 GetPosition() const;

};

