#include "Tracer.h"

Hit Tracer::Ray(Shape* t_shape, const Vector3 t_origin, const Vector3 t_direction)
{
	Hit _temp = Hit();
	t_shape->Intersect(t_origin, t_direction, &_temp);
	return _temp;
}
