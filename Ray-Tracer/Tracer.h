#pragma once
#include "Hit.h"
#include "Shape.h"

class Tracer
{

public:

	static Hit Ray(Shape* t_shape, const Vector3 t_origin, const Vector3 t_direction);
};

