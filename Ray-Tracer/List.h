#pragma once
template <typename T>

// Contains at the moment no buffer.
class List
{
public:

	List();
	List(int t_size);
	~List();

	void Add(T t_object);
	void AddAt(T t_object, int t_index);
	void Remove(const int t_index);
	void Clear();
	int GetSize() const;

	T &operator[](int);

private:

	T* objects;
	// The visual size.
	int size;
	// The actual size, including the buffer.
	int virtualSize;
};

template<typename T>
inline List<T>::List()
{
	objects = new T[0];
	virtualSize = size = 0;
}

template<typename T>
inline List<T>::List(int t_size)
{
	objects = new T[t_size];
	virtualSize = size = t_size;
}

template<typename T>
inline List<T>::~List()
{

}

template<typename T>
void List<T>::Add(T t_object)
{
	size++;
	objects = (T*)realloc(objects, sizeof(T) * size);
	objects[size - 1] = t_object;
}

template<typename T>
inline void List<T>::AddAt(T t_object, int t_index)
{
	size++;
	objects = (T*)realloc(objects, sizeof(T) * size);

	for (int i = size - 1; i > t_index; i--)
	{
		objects[i] = objects[i - 1];
	}

	objects[t_index] = t_object;
}

template<typename T>
inline void List<T>::Remove(const int t_index)
{
	for (int i = t_index; i < size - 1; i++)
	{
		objects[i] = objects[i + 1];
	}

	size--;
	objects = (T*)realloc(objects, sizeof(T) * size);
	
}

template<typename T>
inline void List<T>::Clear()
{
	virtualSize = size = 0;
	objects = (T*)realloc(objects, sizeof(T) * size);
}

template<typename T>
inline int List<T>::GetSize() const
{
	return size;
}

template<typename T>
T& List<T>::operator[](int t_index)
{
	// TODO: insert return statement here
	return objects[t_index];
}
