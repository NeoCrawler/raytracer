#pragma once
#include "GameObject.h"
#include "Tracer.h"
#include "Light.h"
#include "Screen.h"


enum class Projection
{
	Perspective,
	Orthographic
};

class Camera : public GameObject
{

private:
	Projection projection;
	float fieldOfView;
	float focalLength;

	Vector3 forward;
	Vector3 right;
	Vector3 up;
	
public:

	Camera(Vector3 t_position, Vector3 t_direction, Projection t_projection, float t_fov, float t_focalLength);
	
	Vector3 GetViewOrigin(const float t_x, const float t_y) const;
	Vector3 GetViewDirection(const float t_x, const float t_y) const;

};

