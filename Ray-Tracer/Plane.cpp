#include "Plane.h"
#include<iostream>
Plane::Plane(Vector3 t_position, Material* t_material, Vector3 t_normal) : Shape(t_position, t_material)
{
	type = ShapeType::Plane;
	normal = t_normal;
}

bool Plane::Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* t_hit)
{
	float _d = Vector3::Dot(t_direction, normal);
	
	if (abs(_d) >= 1e-6)
	{
		float _b = Vector3::Dot(GetPosition() - t_origin, normal);
		float _t = _b / _d;
		
		if (_t >= 0)
		{
			//std::cout << "hit" << std::endl;
			t_hit->hasHit = true;
			t_hit->shape = this;
			t_hit->material = material;
			t_hit->origin = t_origin;
			t_hit->point = t_origin + t_direction * _t;
			t_hit->normal = normal;
			t_hit->distance = _t;
		}
		else
		{
			t_hit->hasHit = false;
			return false;
		}
	}
	else
	{
		t_hit->hasHit = false;
		return false;
	}

	return false;
}

Vector3 Plane::GetNormal() const
{
	return normal;
}

Box Plane::GetBoundingBox()
{
	Box _temp = Box(GetPosition(), Vector3(-1, -1, -1), Vector3(1, 1, 1));
	return _temp;
}
