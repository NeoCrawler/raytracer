#include "Leaf.h"

Leaf::Leaf(List<Box> t_boxes)
{
	for (size_t i = 0; i < t_boxes.GetSize(); i++)
	{
		shapes.Add(t_boxes[i].GetShape());
	}
}

void Leaf::GetShapes(List<Shape*>* t_result, Vector3 t_origin, Vector3 t_direction)
{
	for (size_t i = 0; i < shapes.GetSize(); i++)
	{
		t_result->Add(shapes[i]);
	}
}
