#define STB_IMAGE_WRITE_IMPLEMENTATION

// new is slow, it first needs to go to the OS and ask for a free memory block.
#pragma once

#include "Camera.h"
#include "Shape.h"
#include "stb_image_write.h"
#include "Sphere.h"
#include "Plane.h"
#include "Cube.h"
#include "Renderer.h"
#include "GameObject.h"
#include "BVH.h"

int main()
{
	Renderer* _renderer = new Renderer(Color(0, 0, 0, 255), Color(50, 50, 50, 255));
	Camera* _camera = new Camera(Vector3(0, 15, -50), Vector3(0, 0, 5), Projection::Perspective, 60.0f, 1.0f);
	
	Shape** _shapes		= new Shape * [2500];
	Shape** _shapesE	= new Shape * [1];
	Light** _lights		= new Light * [1];

	Material _material00 = Material(Color(125, 175, 50, 255), Color(0, 0, 0, 0), Color(255, 255, 255, 255), Color(0, 0, 0, 255) , Color(0, 0, 0, 0));
	Material _material01 = Material(Color(175, 1, 1, 255), Color(0, 0, 0, 0), Color(100, 100, 100, 255), Color(0.75f, 0.75f, 0.75f, 255), Color(0, 0, 0, 0));
	_material01.SetChecker(true);

	for (size_t i = 0; i < 2500; i++)
	{
		Vector3 _position = Vector3(-25 + rand() % 50, 1, -25 + rand() % 50);
		_shapes[i] = new Sphere(_position, &_material00, 1);
	}

	_shapesE[0] = new Plane(Vector3(0, -1, 0), &_material01, Vector3(0, 1, 0));
	_lights[0] = new Light(Vector3(0, 25, 0), 1, Color(1, 1, 1, 255));

	ColorByte* color	= new ColorByte[PIXELS];
	Color* _pixels		= _renderer->Render(_camera, _shapes, _shapesE, _lights);

	// Convert rendered colors to colors we can draw.
	for (size_t i = 0; i < PIXELS; i++)
	{
		color[i] = _pixels[i].GetColorInBytes();
	}

	std::cout << "Complete" << std::endl;
	stbi_write_png("01_Test.png", 1920, 1080, 4, color, 1920 * 4); 
	system("01_Test.png");
};