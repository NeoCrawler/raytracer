#include "Color.h"

float* Color::GetColor() const
{
	return new float[4]{ red, green, blue, alpha };
}

ColorByte Color::GetColorInBytes() const
{
	ColorByte byte = ColorByte();
	byte.red	= static_cast<unsigned char>(red);
	byte.blue	= static_cast<unsigned char>(blue);
	byte.green	= static_cast<unsigned char>(green);
	byte.alpha	= static_cast<unsigned char>(alpha);
	
	return byte;
}

void Color::Clamp()
{
	red		= Math::Clamp(red, 0, 255);
	green	= Math::Clamp(green, 0, 255);
	blue	= Math::Clamp(blue, 0, 255);
	alpha	= Math::Clamp(alpha, 0, 255);
}

void Color::SetColor(float a, float r, float g, float b)
{
	red = r;
	green = g;
	blue = b;
	alpha = a;
}
