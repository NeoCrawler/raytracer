#pragma once
#include "GameObject.h"
#include "Box.h"

/// Compiler advised me to use enum class.
/// Turns out it results in fewer suprise bugs.
/// Since the content is now local to the enumerator.

enum class ShapeType
{
	None,
	Box,
	Sphere,
	Plane,
};

class Shape : public GameObject
{

protected:

	ShapeType type;
	Material* material; // Pointer or not?

public:

	Shape(Vector3 t_position, Material* t_material);

	virtual bool Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* t_hit) = 0;
	virtual Box GetBoundingBox() = 0;

	const ShapeType GetShapeType() const;
	const Material* GetMaterial() const;
	
};

