#pragma once
#include "Shape.h"

class Plane : public Shape
{

private:

	Vector3 normal;

public:

	Plane(Vector3 t_position, Material* t_material, Vector3 t_normal);
	
	// Plane intersection.
	virtual bool Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* t_hit) override;
	
	Vector3 GetNormal() const;

	// Inherited via Shape
	virtual Box GetBoundingBox() override;
};

