#include "BVH.h"
#include <iostream>
BVH::BVH(Shape** t_shapes, int t_size)
{	
	List<Box> _boxes;

	for (int i = 0; i < t_size; i++)
	{
		Box _temp = t_shapes[i]->GetBoundingBox();
		_temp.SetShape(t_shapes[i]);

		if (_boxes.GetSize() > 0)
		{
			
			for (int j = 0; j < _boxes.GetSize(); j++)
			{
				if (_temp.GetPosition().z < _boxes[j].GetPosition().z)
				{
					_boxes.AddAt(_temp, j);
					break;
				}
				else if (_temp.GetPosition().z == _boxes[j].GetPosition().z)
				{
					_boxes.AddAt(_temp, j);
					break;
				}
				else if (j == _boxes.GetSize() - 1)
				{
					_boxes.Add(_temp);
					break;
				}
			}
		}
		else
		{
			_boxes.Add(_temp);
		}
	}

#if _DEBUG
	// Print the sorting outcome.
	for (size_t i = 0; i < _boxes.GetSize(); i++)
	{
		printf("%f\n", _boxes[i].GetPosition().z);
	}
#endif //  DEBUG

	GenerateBounding(_boxes);

	Split(_boxes);
}
