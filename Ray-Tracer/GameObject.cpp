#include "GameObject.h"

GameObject::GameObject(Vector3 t_position)
{
	position = t_position;
}

void GameObject::SetPosition(Vector3 t_position)
{
	position = t_position;
}

Vector3 GameObject::GetPosition() const
{
	return position;
}

