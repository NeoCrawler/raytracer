#pragma once
#include "Shape.h"

class Sphere : public Shape
{

private:

	float radius;

public:

	Sphere(Vector3 t_position, Material* t_material, float t_radius);
	
	// Sphere intersection.
	virtual bool Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* hit) override;

	float GetRadius() const;

	// Inherited via Shape
	virtual Box GetBoundingBox() override;
};

