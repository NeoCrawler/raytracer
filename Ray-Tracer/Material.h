#pragma once
#include "Color.h"
class Vector3;


class Material
{

private:

	Color diffuse;
	Color ambient;
	Color specular;
	Color reflection;
	Color emission;
	bool isChecker;

public:

	void SetChecker(bool t_isChecker);

	Material();
	Material(Color t_diffuse, Color t_ambient, Color t_specular, Color t_reflection, Color t_emission);
	const Color& GetDiffuse(const Vector3 t_point) const;
	const Color& GetAmbient() const;
	const Color& GetSpecular() const;
	const Color& GetEmission() const;
	const Color& GetReflection() const;
};

