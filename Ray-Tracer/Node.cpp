#include "Node.h"

void Node::Split(List<Box> t_boxes)
{
	List<Box> _left;
	List<Box> _right;

	int _half = static_cast<int>(ceilf(t_boxes.GetSize() * .5f));

	for (int i = 0; i < _half; i++)
	{
		_left.Add(t_boxes[i]);
	}

	for (int i = _half; i < t_boxes.GetSize(); i++)
	{
		_right.Add(t_boxes[i]);
	}

	// Left side (higer node count).
	if (_left.GetSize() < 3)
	{
		left = new Leaf(_left);
	}
	else
	{
		left = new Node(_left);
	}

	// Right side (smaller node count).
	if (_right.GetSize() < 3)
	{
		right = new Leaf(_right);
	}
	else
	{
		right = new Node(_right);
	}
}

void Node::GenerateBounding(List<Box> t_boxes)
{
	bounding.min = t_boxes[0].GetPosition() + t_boxes[0].min;
	bounding.max = t_boxes[t_boxes.GetSize() - 1].GetPosition() + t_boxes[t_boxes.GetSize() - 1].max;

	for (int i = 1; i < t_boxes.GetSize() - 1; i++)
	{
		if (bounding.GetPosition().x + bounding.min.x > t_boxes[i].GetPosition().x + t_boxes[i].min.x)
			bounding.min.x = t_boxes[i].GetPosition().x + t_boxes[i].min.x;

		if (bounding.GetPosition().y + bounding.min.y > t_boxes[i].GetPosition().y + t_boxes[i].min.y)
			bounding.min.y = t_boxes[i].GetPosition().y + t_boxes[i].min.y;

		if (bounding.GetPosition().x + bounding.max.x < t_boxes[i].GetPosition().x + t_boxes[i].max.x)
			bounding.max.x = t_boxes[i].GetPosition().x + t_boxes[i].max.x;

		if (bounding.GetPosition().y + bounding.max.y < t_boxes[i].GetPosition().y + t_boxes[i].max.y)
			bounding.max.y = t_boxes[i].GetPosition().y + t_boxes[i].max.y;
	}

	bounding.SetPosition(bounding.max - bounding.min);
}

Node::Node()
{
	left = nullptr;
	right = nullptr;
}

Node::Node(List<Box> t_boxes)
{
	GenerateBounding(t_boxes);
	Split(t_boxes);
}

void Node::GetShapes(List<Shape*>* t_result, Vector3 t_origin, Vector3 t_direction)
{
	if (bounding.Intersect(t_origin, t_direction))
	{
		left->GetShapes(t_result, t_origin, t_direction);
		right->GetShapes(t_result, t_origin, t_direction);
	}
}
