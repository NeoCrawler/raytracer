#pragma once
#include "BaseNode.h"
#include "Shape.h"
class Leaf : public BaseNode
{

private:

	List<Shape*> shapes;

public:

	Leaf(List<Box> t_boxes);

	// Inherited via BaseNode
	virtual void GetShapes(List<Shape*>* t_result, Vector3 t_origin, Vector3 t_direction) override;
};

