#pragma once
#include "GameObject.h"

class Light : public GameObject
{
private:

	float intensity;
	Color color;

public:

	Light(Vector3 t_position, float t_intensity, Color t_color);
	void SetIntensity(float t_intensity);
	float GetIntensity() const;
	void SetColor(Color t_color);
	const Color* GetColor() const;
};

