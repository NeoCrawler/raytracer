#include "Light.h"

Light::Light(Vector3 t_position, float t_intensity = 10, Color t_color = Color(255,255,255,255)) : GameObject(t_position)
{
	intensity = t_intensity;
	color = t_color;
}

void Light::SetIntensity(float t_intensity)
{
	intensity = t_intensity;
}

float Light::GetIntensity() const
{
	return intensity;
}

void Light::SetColor(Color t_color)
{
	color = t_color;
}

const Color* Light::GetColor() const
{
	return &color;
}
