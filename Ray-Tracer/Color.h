#pragma once
#include "MyMath.h"

// Color struct in bytes.
class ColorByte
{
public:
	ColorByte() : red(0) , green(0), blue(0), alpha(0){}
	
	unsigned char red, green, blue, alpha;
};

// Represent a color in RGBA
// Convert color member data to Vector4 later on when such Vector class exists.
class Color
{
public:
	float red, green, blue, alpha;

	Color(float r = 0, float g = 0, float b = 0, float a = 0) : red(r), green(g), blue(b), alpha(a) {}

	Color operator*(const float scalar) const
	{
		Color _temp = Color(red * scalar, green * scalar, blue * scalar, alpha);
		_temp.Clamp();
		return _temp;
	}

	Color operator*(const Color rhs) const
	{
		Color _temp = Color(red * rhs.red, green * rhs.green, blue * rhs.blue, alpha);
		_temp.Clamp();
		return _temp;
	}

	void operator+=(const Color rhs)
	{
		red += rhs.red;
		green += rhs.green;
		blue += rhs.blue;
		alpha = 255;
		Clamp();
	}

	Color operator+(const Color rhs) const
	{
		Color _temp = Color(red + rhs.red, green + rhs.green, blue + rhs.blue, alpha);
		_temp.Clamp();
		return _temp;
	}

	Color operator-(const Color rhs) const
	{
		Color _temp = Color(red - rhs.red, green - rhs.green, blue - rhs.blue, alpha);
		_temp.Clamp();
		return _temp;
	}

	void SetColor(float a = 0, float r = 0, float g = 0, float b = 0);

	// Returns RGBA
	float* GetColor() const;
	ColorByte GetColorInBytes() const;

private:

	

	void Clamp();
};

