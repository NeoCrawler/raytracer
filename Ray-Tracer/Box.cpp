#include "Box.h"

Box::Box() : GameObject(Vector3())
{
	shape = nullptr;
}

Box::Box(Vector3 t_position, Vector3 t_min, Vector3 t_max) : GameObject(t_position)
{
	shape = nullptr;
	min = t_min;
	max = t_max;
}

bool Box::Intersect(Vector3 t_origin, Vector3 t_direction)
{
    float tmin = (min.x - t_origin.x) / t_direction.x;
    float tmax = (max.x - t_origin.x) / t_direction.x;

    if (tmin > tmax) Math::Swap(tmin, tmax);

    float tymin = (min.y - t_origin.y) / t_direction.y;
    float tymax = (max.y - t_origin.y) / t_direction.y;

    if (tymin > tymax) Math::Swap(tymin, tymax);

    if ((tmin > tymax) || (tymin > tmax))
        return false;

    if (tymin > tmin)
        tmin = tymin;

    if (tymax < tmax)
        tmax = tymax;

    float tzmin = (min.z - t_origin.z) / t_direction.z;
    float tzmax = (max.z - t_origin.z) / t_direction.z;

    if (tzmin > tzmax) Math::Swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
        return false;

    if (tzmin > tmin)
        tmin = tzmin;

    if (tzmax < tmax)
        tmax = tzmax;

    return true;
}

Shape* Box::GetShape()
{
	return shape;
}

void Box::SetShape(Shape* t_shape)
{
    shape = t_shape;
}
