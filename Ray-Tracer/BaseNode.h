#pragma once

#include "List.h"
#include "Box.h"
class Shape;


class BaseNode
{
public:

	virtual void GetShapes(List<Shape*>* t_result, Vector3 t_origin, Vector3 t_direction) = 0;
};

