#include "Cube.h"

Cube::Cube(Vector3 t_position, Material* t_material, Vector3 t_size) : Shape(t_position, t_material)
{
    size = t_size;
    min[0] = GetPosition() - (size * 0.5f);
    min[1] = GetPosition() + (size * 0.5f);
}

bool Cube::Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* t_hit)
{
    float tmin = (min[0].x - t_origin.x) / t_direction.x;
    float tmax = (min[1].x - t_origin.x) / t_direction.x;

    if (tmin > tmax) Math::Swap(tmin, tmax);

    float tymin = (min[0].y - t_origin.y) / t_direction.y;
    float tymax = (min[1].y - t_origin.y) / t_direction.y;

    if (tymin > tymax) Math::Swap(tymin, tymax);

    if ((tmin > tymax) || (tymin > tmax))
        return false;

    if (tymin > tmin)
        tmin = tymin;

    if (tymax < tmax)
        tmax = tymax;

    float tzmin = (min[0].z - t_origin.z) / t_direction.z;
    float tzmax = (min[1].z - t_origin.z) / t_direction.z;

    if (tzmin > tzmax) Math::Swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
        return false;

    if (tzmin > tmin)
        tmin = tzmin;

    if (tzmax < tmax)
        tmax = tzmax;

    t_hit->hasHit = true;
    t_hit->material = material;
    t_hit->point = t_origin + t_direction * tmin;
    t_hit->normal = (t_hit->point - GetPosition()).Normalized();
    t_hit->origin = t_origin;
    t_hit->shape = this;
    t_hit->distance = tmin;

    Vector3 normal;
    Vector3 localPoint = t_hit->point - GetPosition();
    float min = FLT_MAX;
    float distance = abs(size.x - abs(localPoint.x));
    if (distance < min) {
        min = distance;
        normal = Vector3(-1, 0, 0);
        normal *= Math::Sign(localPoint.x);
    }
    distance = abs(size.y - abs(localPoint.y));
    if (distance < min) {
        min = distance;
        normal = Vector3(0, -1, 0);
        normal *= Math::Sign(localPoint.y);
    }
    distance = abs(size.z - abs(localPoint.z));
    if (distance < min) {
        min = distance;
        normal = Vector3(0, 0, -1);
        normal *= Math::Sign(localPoint.z);
    }
    
    t_hit->normal = normal;

    return true;
}

Box Cube::GetBoundingBox()
{
    Box _temp = Box(GetPosition(), Vector3(-1, -1, -1), Vector3(1, 1, 1));
    return _temp;
}
