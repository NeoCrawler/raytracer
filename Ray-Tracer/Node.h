#pragma once
#include "BaseNode.h"
#include "Leaf.h"

class Node : public BaseNode
{

protected:

	BaseNode* left;
	BaseNode* right;
	Box bounding;

	void Split(List<Box> t_boxes);
	void GenerateBounding(List<Box> t_boxes);

public:

	Node();
	Node(List<Box> t_boxes);

	// Inherited via BaseNode
	virtual void GetShapes(List<Shape*>* t_result, Vector3 t_origin, Vector3 t_direction) override;
};

