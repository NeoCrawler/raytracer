#pragma once
class Material;
class Shape;
#include "Vector3.h"

struct Hit
{

public:
	
	bool hasHit			= false;
	Shape* shape		= nullptr;
	Material* material	= nullptr;
	Vector3 origin		= Vector3::Zero();
	Vector3 point		= Vector3::Zero();
	Vector3 normal		= Vector3::Zero();
	float distance		= 0;
};

