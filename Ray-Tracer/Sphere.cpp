#include "Sphere.h"
#include <iostream>
Sphere::Sphere(Vector3 t_position, Material* t_material, float t_radius) : Shape(t_position, t_material)
{
	type = ShapeType::Sphere;
	radius = t_radius;
}

bool Sphere::Intersect(const Vector3 t_origin, const Vector3 t_direction, Hit* t_hit)
{
	//float _a = 1;
	float _b = Vector3::Dot(t_direction, t_origin - GetPosition());
	float _c = Vector3::Dot((t_origin - GetPosition()), (t_origin - GetPosition())) - powf(radius, 2);
	float _d = powf(_b, 2) - _c;

	// _t > 0 will always render the closest side to the camera.
	if (_d >= 0)
	{
		float _t0 = (Vector3::Dot(t_direction.Inverse(), t_origin - GetPosition())) - sqrt(_d);
		//float _t1 = (Vector3::Dot(t_direction.Inverse(), t_origin - GetPosition())) + sqrt(_d) / _a;

		/*if (_t1 < _t0)
			_t0 = _t1;*/

		if (_t0 >= 0)
		{
			t_hit->hasHit = true;
			t_hit->shape = this;
			t_hit->material = material;
			t_hit->origin = t_origin;
			t_hit->point = t_origin + t_direction * _t0;
			t_hit->normal = (t_hit->point - GetPosition()).Normalized();
			t_hit->distance = _t0;

			return true;
		}
		else
		{
			t_hit->hasHit = false;
			return false;
		}
	}
	else
	{
		t_hit->hasHit = false;
		return false;
	}
	
		
}

float Sphere::GetRadius() const
{
	return radius;
}

Box Sphere::GetBoundingBox()
{
	Box _temp = Box(GetPosition(), Vector3(-1, -1, -1), Vector3(1, 1, 1));
	return _temp;
}


// Just check whether I'm am myself. Not offset the distance. Will have effect in the long run.