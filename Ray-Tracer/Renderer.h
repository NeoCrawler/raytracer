#pragma once
#include <iostream>

#include "Light.h"
#include "Shape.h"
#include "Color.h"
#include "Camera.h"
#include "Screen.h"
#include "MyMath.h"
#include "BVH.h"

class Renderer
{
public:
	Renderer(Color t_skyColor, Color t_ambientColor);
	Color* Render(Camera*, Shape** t_shapes, Shape** t_shapesE, Light** t_lights);

private:

	BVH* bvh;
	List<Shape*>* shapes;
	List<Shape*>* shapesExcluded;
	Color skyColor;
	Color ambientColor;

	Color GetLambart(List<Shape*>* t_shapes, Light** t_lights, const Hit t_hit, const int t_current);
	Color GetSpecular(List<Shape*>* t_shapes, Light** t_lights, const Hit t_hit, const int t_current);
	Color GetReflection(List<Shape*>* t_shapes, Light** t_lights, const Hit t_hit, const int t_depth);
	Color GetRefraction(List<Shape*> t_shapes, Light** t_lights, const Hit t_hit, const int t_depth);
};

