#include "Camera.h"

Camera::Camera(Vector3 t_position, Vector3 t_direction, Projection t_projection, float t_fov, float t_focalLength) : GameObject(t_position)
{
	projection = t_projection;
	fieldOfView = t_fov;
	focalLength = t_focalLength;

	forward = (t_direction - GetPosition()).Normalized();
	right = Vector3::Cross(forward, Vector3::Up()).Normalized();
	up = Vector3::Cross(right, forward);
}

Vector3 Camera::GetViewOrigin(const float t_x, const float t_y) const
{
	if (projection == Projection::Perspective)
	{
		return GetPosition();
	}
	else 
	{
		float _u = Math::Remap(t_x, 0, WIDTH, 1, -1);
		float _v = Math::Remap(t_y, 0, HEIGHT, 1, -1);

		float _aspectRatio = static_cast<float>(WIDTH) / static_cast<float>(HEIGHT);

		return GetPosition() + right * _u * _aspectRatio + up * _v;
	}
}

Vector3 Camera::GetViewDirection(const float t_x, const float t_y) const
{
	

	if (projection == Projection::Perspective)
	{
		float _a = tanf((fieldOfView / 2) * (static_cast<float>(Pi) / 180));

		// Remapping to viewport values.
		float _u = Math::Remap(t_x, 0, WIDTH, _a, -_a);
		float _v = Math::Remap(t_y, 0, HEIGHT, _a, -_a);

		float _aspectRatio = static_cast<float>(WIDTH) / static_cast<float>(HEIGHT);

		return (forward + right * _u * _aspectRatio + up * _v).Normalized();
	}
	else
	{
		return forward;
	}
}