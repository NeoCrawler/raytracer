#include "Renderer.h"

Renderer::Renderer(Color t_skyColor, Color t_ambientColor)
{
	bvh				= nullptr;
	shapes			= nullptr;
	shapesExcluded	= nullptr;
	skyColor = t_skyColor;
	ambientColor = t_ambientColor;
}

Color* Renderer::Render(Camera* t_camera, Shape** t_shapes, Shape** t_shapesE, Light** t_lights)
{
	if (!bvh)
		bvh = new BVH(t_shapes, 2500);

	if (!shapesExcluded)
	{
		shapesExcluded = new List<Shape*>();

		for (size_t i = 0; i < 1; i++)
		{
			shapesExcluded->Add(t_shapesE[i]);
		}
	}
		

	Color* _pixels = new Color[PIXELS];
	
	for (int v = 0; v < HEIGHT; v++)
	{
		for (int u = 0; u < WIDTH; u++)
		{
			float _u = static_cast<float>(u);
			float _v = static_cast<float>(v);

			Color _pixel = skyColor;
			int _index = u + (v * WIDTH);

			Vector3 _origin = t_camera->GetViewOrigin(_u, _v);
			Vector3 _direction = t_camera->GetViewDirection(_u, _v);

			if (shapes)
			{
				shapes->Clear();
				delete shapes;
			}
			
			shapes = new List<Shape*>();
			bvh->GetShapes(shapes, _origin, _direction);

			for (size_t i = 0; i < shapesExcluded->GetSize(); i++)
			{
				shapes->Add(shapesExcluded->operator[](i));
			}

			float _depth = FLT_MAX;

			// Loop through all shapes.
			for (int i = 0; i < shapes->GetSize(); i++)
			{
				
				Hit _hit = Tracer::Ray(shapes->operator[](i), _origin, _direction);

				// Shape's surface is hit.
				if (_hit.hasHit)
				{
					// Pixel on surface is the closest to the camera.
					if (_hit.distance < _depth)
					{
						//_pixel = Color(0, 50, 100, 255);
						_depth = _hit.distance;

						Color _shading		= GetLambart(shapes, t_lights, _hit, i);
						Color _specular		= GetSpecular(shapes, t_lights, _hit, i);
						Color _reflection	= GetReflection(shapes, t_lights, _hit, 2);

						_pixel.red = Math::Lerp(_shading.red, _reflection.red, _hit.material->GetReflection().red);
						_pixel.green = Math::Lerp(_shading.green, _reflection.green, _hit.material->GetReflection().green);
						_pixel.blue = Math::Lerp(_shading.blue, _reflection.blue, _hit.material->GetReflection().blue);

						_pixel += _specular;
						//Color _refraction	= GetRefraction(t_shapes, t_lights, _hit, 4);
					}
				}
				
				
			}

			// If nothing was hit, lets draw a background.
			/*if (_hits == 0)
			{
				float _temp = Math::Remap(v + u, 0, WIDTH + HEIGHT, 0, 255);
				_pixel = Color(_temp, _temp, _temp, 255);
			}*/
			_pixel += Color(25, 25, 25, 255); // Add ambient color only once.
			_pixels[_index] = _pixel;

		}
	}

	return _pixels;
}

Color Renderer::GetLambart(List<Shape*>* t_shapes, Light** t_lights, const Hit t_hit, const int t_current)
{
	Color _temp = Color(0, 0, 0, 255);

	if (t_hit.hasHit)
	{
		// Do lambert shading.
		for (int l = 0; l < 1; l++)
		{
			float _blockage = 1;

			// Check whether there is an object blocking the light direction.
			for (int s = 0; s < t_shapes->GetSize(); s++)
			{
				Hit _shadow = Tracer::Ray(t_shapes->operator[](s), t_hit.point, (t_lights[l]->GetPosition() - t_hit.point).Normalized());

				float _lightD = Vector3::Distance(t_hit.point, t_lights[l]->GetPosition());
				float _ObjectD = Vector3::Distance(t_hit.point, _shadow.point);

				if (_shadow.hasHit && _ObjectD <= _lightD && _shadow.shape != t_hit.shape)
				{
					_blockage = 0;
				}
			}

			float _i = t_lights[l]->GetIntensity();

			Vector3 _n = t_hit.normal;												// Hit normal.
			Vector3 _l = (t_lights[l]->GetPosition() - t_hit.point).Normalized();	// Direction from point to light.

			Color _lightColor = *t_lights[l]->GetColor();
			Color _diffuseColor = (t_shapes->operator[](t_current)->GetMaterial()->GetDiffuse(t_hit.point)) * Math::Max(0, Vector3::Dot(_n, _l));

			_temp += (_diffuseColor) * (_lightColor * _i) * _blockage;
		}
	}
	else {
		_temp = Color(0, 50, 150);
	}
	

	return _temp;
}

Color Renderer::GetSpecular(List<Shape*>* t_shapes, Light** t_lights, const Hit t_hit, const int t_current)
{
	Color _temp;

	// Do lambert shading.
	for (int l = 0; l < 1; l++)
	{
		Vector3 _n = t_hit.normal;												// Hit normal.
		Vector3 _l = (t_lights[l]->GetPosition() - t_hit.point).Normalized();	// Direction from point to light.
		Vector3 _v = (t_hit.origin - t_hit.point).Normalized();					// Direction from point to camera.
		Vector3 _h = (_l + _v).Normalized();

		Color _specularColor = t_shapes->operator[](t_current)->GetMaterial()->GetSpecular() * powf(Math::Max(0, Vector3::Dot(_n, _h)), 50);
		_temp += _specularColor;
	}

	return _temp;
}

Color Renderer::GetReflection(List<Shape*>* t_shapes, Light** t_lights, const Hit t_hit, const int t_depth)
{
	Color _temp;

	Color _reflectivity = Color(1, 1, 1, 255);

	Hit _preRefl = t_hit;
	Hit _postRefl = t_hit;
	_postRefl.hasHit = false;

	float _reflectionDepth = FLT_MAX;
	int _target = 0;

	Color _reflectionStrength = Color(1, 1, 1, 255);

	int _max = 10;
	for (int r = 1; r <= t_depth; r++)
	{

		if (r < _max)
		{
			Vector3 _v = (_preRefl.point - _preRefl.origin).Normalized();
			Vector3 _r = _v - _preRefl.normal * (2 * Vector3::Dot(_v, _preRefl.normal));
			Hit _reflect;

			/*delete t_shapes;
			bvh->GetShapes(t_shapes, _preRefl.point, _r);
			for (size_t i = 0; i < shapesExcluded->GetSize(); i++)
			{
				shapes->Add(shapesExcluded->operator[](i));
			}*/

			for (int o = 0; o < t_shapes->GetSize(); o++)
			{
				_reflect = Tracer::Ray(t_shapes->operator[](o), _preRefl.point, _r);
				
				// Have we hit an object with the reflection ray.
				if (_reflect.hasHit && _reflect.shape != _preRefl.shape)
				{
					// Is this hit the closest hit?
					if (_reflect.distance < _reflectionDepth)
					{
						_reflectionDepth = _reflect.distance;
						_target = o;
						_postRefl = _reflect;
					}
				}
			}

			if (_postRefl.hasHit)
			{
				//_reflectivity = GetLambart(t_shapes, t_lights, _postRefl, _target);
				_reflectionStrength = _reflectionStrength - (Color(1, 1, 1, 255) - _preRefl.material->GetReflection());
				_preRefl = _postRefl;
				_temp += _reflectivity * _reflectionStrength;
				
			}
			else
			{
				_temp += skyColor;
				r = _max;
				//std::cout << r << std::endl;
			}

			
		}
		else
		{
			r = _max;
		}
	}

	return _temp;
}

Color Renderer::GetRefraction(List<Shape*> t_shapes, Light** t_lights, const Hit t_hit, const int t_depth)
{
	Color _temp = Color(0, 0, 0, 255);

	Vector3 _direction = (t_hit.point - t_hit.origin).Normalized();

	float _n = 1.4f;
	float _nt = 1.4f;
	float _dot = Vector3::Dot(_direction, t_hit.normal);

	float _d = 1 - ((_n * _n) * (1 - _dot * _dot)) / (_nt * _nt);

	Vector3 _t = (_direction + t_hit.normal * _dot) * _n / _nt;
	_t - t_hit.normal * sqrtf(_d);

	for (int i = 0; i < t_depth; i++)
	{
		if (_d > 0)
		{
			for (int j = 0; j < 25; j++)
			{

				Hit _refracted = Tracer::Ray(t_shapes[j], t_hit.point, _t);
				_refracted.normal.Inverse();

				if (_refracted.hasHit && _refracted.shape != t_hit.shape)
				{
					//_temp = GetLambart(t_shapes, t_lights, _refracted, j);
				}
			}
		}
	}

	return _temp;
}
